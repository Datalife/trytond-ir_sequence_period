datalife_ir_sequence_period
===========================

The ir_sequence_period module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-ir_sequence_period/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-ir_sequence_period)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
