# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import ir
from . import sale
from . import purchase
from . import stock


def register():
    Pool.register(
        ir.Sequence,
        ir.SequencePeriod,
        ir.SequenceStrict,
        module='ir_sequence_period', type_='model')
    Pool.register(
        sale.Sale,
        module='ir_sequence_period', type_='model',
        depends=['sale'])
    Pool.register(
        purchase.Purchase,
        module='ir_sequence_period', type_='model',
        depends=['purchase'])
    Pool.register(
        stock.ShipmentOut,
        stock.ShipmentOutReturn,
        stock.ShipmentIn,
        stock.ShipmentInReturn,
        stock.ShipmentInternal,
        module='ir_sequence_period', type_='model',
        depends=['stock'])
